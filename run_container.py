#!/usr/bin/python3

import logging

from conu import DockerRunBuilder, DockerBackend

# our webserver will be accessible on this port
port = 80

# we'll utilize this container image
#image_name = "docker.io/library/nginx"
image_name = "registry.opensuse.org/opensuse/tumbleweed"
image_tag = "latest"

# we'll run our container using docker engine
with DockerBackend(logging_level=logging.DEBUG) as backend:
    # the image will be pulled if it's not present
    image = backend.ImageClass(image_name, tag=image_tag)

    # the command to run in a container
    command = ["pwd"]
     #b = conu.DockerRunBuilder(command=["sleep", "infinity"])
     #   b.options += [
    # let's run the container (in the background)
    container = image.run_via_binary(volumes=("/home/iob/Documents/Programming","/opt"),
                                     additional_opts=["--net=host",
                                                      "--pid=host",
                                                      "--ipc=host",
                                                      "-it",
                                                      "--privileged",
                                                      "-w" "/opt"])
    try:
        container.execute(["ls","-la","/opt/imager"])
        container.execute(["zypper","-n","install","python3-pip"])
        container.execute(["ls","-la","/opt/imager"])
        container.execute(["pip","install","-r","/opt/imager/requirements.txt"])
        container.execute(["pytest", "-v", "--rootdir=/opt/imager/tests", "-k", "container"])
        #, "--junitxml=report.xml"])
        
        #with container.mount() as fs:
        #    assert 'root:x:0:0:root:/root:' in fs.read_file("/etc/passwd")
    finally:
        container.kill()
        container.delete()
