# imager

`imager` utilizes the conu framework to run container test inside a running container.
The testing is in the form of the Unit test and it can be grouped per py file as pytest framework does.

The intention is to create a dedicated framework to integrate and run tests in OpenQA but it also can
run as a standalone tool.

This provides faster execution and cleanier structure compare to OpenQA approach on container images.

## Installation

```
cd ~/src
git clone git@gitlab.com:b10n1k/imager.git
cd imager
pip install -r requirements.txt
```

## Usage

**Make sure that the docker service is up and running**

```
python3 imager.py --img registry.opensuse.org/opensuse/tumbleweed --tag latest --rootpath ~/src
```

`rootpath` should be the path to the `imager` repo. for instance in the example above the repo path is
`~/src/imager`

## Adding Tests

add any group of tests under `tests` folder.

## TODO
- podman runtime
