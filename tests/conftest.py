import pytest
import logging
import subprocess
from conu import DockerRunBuilder, DockerBackend
from conu import Probe, ProbeTimeout
from imager.config import Config
import time

def pytest_addoption(parser):
    logging.info('Parse parameters')
    parser.addoption("--img", action="store", help="the SUT container image")
    parser.addoption("--tag", action="store", help="the SUT container image tag")
    parser.addoption("--rootpath", action="store", help="the pytest rootpath variable")
    
@pytest.fixture(scope="session")
def container(request):
   image_name = request.config.getoption("--img")
   image_tag = request.config.getoption("--tag")
   base_path = request.config.getoption("--rootpath")
   # TODO 
   if not (image_name and image_tag):
       return
   # we'll run our container using docker engine
   with DockerBackend(logging_level=logging.DEBUG) as backend:
       # the image will be pulled if it's not present
       image = backend.ImageClass(image_name, tag=image_tag)
       # let's run the container (in the foreground)
       
       container = image.run_via_binary(volumes=[(base_path, "/opt"), ("/tmp","/tmp")],
                                                      additional_opts=["-i",
                                                                       "--privileged",
                                                                       "-w=/opt"
                                                                       ],
                                                     # popen_params={"stdin": subprocess.PIPE,
                                                     #               "stdout": subprocess.PIPE}
                                                      )
       #p = Probe(timeout=9)
       #p.run()
       time.sleep(5)
       container.execute(["zypper","-n","install","python3-pip"])
       container.execute(["pip","install","-r","/opt/imager/requirements.txt"])
       #container.execute(["pytest", "-v", "--rootdir=/opt/imager/tests", "-k", "container"])
       #return container
       yield container
       print('******TEARDOWN******')
       container.stop()
       container.delete(force=True)

    
@pytest.fixture(scope='session')
def runner_cfg():
    cfg = Config()
    return cfg
